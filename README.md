
### What is this repository for? ###
This repo is for the learning and inference of multi-fingered grasps of our isrr 2017 grasp planning paper. 
The learning takes the RGBD image of the object and the grasp configuration as inputs to predict a successful lable. 
The inference infers high quality grasp configrations from the RGBD image of the object.

### Branches ####
sim_grasp_experiment branch is for simulation multi-fingered grasp experiments. Master branch is the same with sim_grasp_experiment right now. 
real_robot_grasp_exeperiment is for real robot multi-fingered experiments.

### Dependencies ###

Tensorflow 1.1.0  
Opencv 2.4.8  
PCL 1.7  
ROS Indigo  
Our grasp pipeline code 

### Trained models ###

models folder: trained models for seen objects.     
models_part folder: trained models for unseen objects.
